# Exchange node SDK

Node SDK for INCX Exchange

## Setup

```bash
# install dependencies
npm install exchange-node-sdk
```

## Usage

Config INCX Exchange Node SDK like below.

```js
const Incx = require("exchange-node-sdk");

const config = {
  baseUrl: BASE_URL,
  userId: USER_ID,
  apiKey: API_KEY,
  apiSecret: API_SECRET,
};
const incxSdk = new Incx(config);
```

This setup will create an instance of INCX Exchange Node SDK with the provided config. All config properties are required.

## SDK for User's Orders

You can use the following syntax to fetch, create, and delete user's orders.

### Get User Orders

This is used to get a list of user orders returning a promise.

```js
const incxSdk = new Incx(config);

const options = {
    fromDate: new Date('2019-02-01T23:48:35.708Z'),
    toDate: new Date('2019-02-11T23:48:35.708Z'),
    limit: 25,
    state: 'OPEN',
}

const userOrders = incxSdk.getOrders(options).then(response => {
  return response;
});
```

Options

```js
{
    fromDate: Date, // Optional: Latest order time

    toDate: Date, // Optional: Earliest order time

    limit: Number, // Optional: Number of orders return per query

    state: String, // Optional: Order state ("OPEN", "CLOSED"), if no state provided return all orders
}
```

Response

```js
[
    {
        base_account_id: String,
        canary_debug: Boolean,
        commission: String,
        created_time: String,   // ISO time in UTC
        expire_time: String,   // ISO time in UTC
        fee_credit_account_id: String,
        fee_currency: String,
        fee_debit_account_id: String,
        fees: String,
        maker_only: Boolean,
        order_id: String,
        order_type: String,
        partner_fee_credit_account_id: String,
        partner_fee_currency: String,
        partner_fees: String,
        price: String,
        quantity: String,
        quote_account_id: String,
        quote_coin_amount: String,
        remaining_quantity: String,
        side: String,
        status: String,
        status_invalid_reason: String,
        ticker: String,
        updated_time: String,   // ISO time in UTC
        user_id: String,
    }
]
```

### Get User Order

Get a user order of provided id.

```js
const incxSdk = new Incx(config);

const userOrders = incxSdk.getOrder(orderId).then(response => {
  return response;
});
```

Params
```js
{
    orderId: String, // target order id
}
```

### Create Order

Create new order

```js
const incxSdk = new Incx(config);

const userOrders = incxSdk.createOrder(data).then(response => {
  return response;
});
```

Params
```js
{
    data: {
        marker_only: Boolean, // Optional, default: false
        order_type: String, // Optional, default: "LIMIT"
        price: String, // Number string in 8 decimal places
        quantity: String, // Number string in 8 decimal places
        side: String, // "BUY" or "SELL"
        ticker: String, // Ex: "ETH/BTC"
    },
}
```

### Cancel Order

Cancel order

```js
const incxSdk = new Incx(config);

const userOrders = incxSdk.cancelOrder(orderId).then(response => {
  return response;
});
```

Params
```js
{
    orderId: String, // target order id
}
```