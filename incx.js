const querystring = require("querystring");

const utils = require("./utils/utils");
const {
  API_KEY_HEADER,
  NONCE_HEADER,
  SIGNATURE_HEADER,
  SIGNED_HEADER,
  HOST_HEADER,
  GET_USER_ORDERS,
  GET_USER_ORDER,
  CREATE_USER_ORDER,
  DELETE_USER_ORDER,
  CONTENT_TYPE_HEADER,
  APPLICATION_JSON,
} = require("./utils/constants");

class Incx {
  /**
   *
   * @param {Object.<string, Object>} config
   */
  constructor(config) {
    utils.validateConfig(config);
    this.BASE_URL = config.baseUrl;
    this.USER_ID = config.userId;
    this.API_KEY = config.apiKey;
    this.API_SECRET = config.apiSecret;
  }

/**
 * 
 * @param {Object.<string, *>=} options 
 */
  getOrders(options) {
    const { fromDate, toDate, limit, state } = options || {};
    const queries = {};
    if (fromDate instanceof Date) {
      queries.fromDate = fromDate.toISOString();
    }
    if (toDate instanceof Date) {
      queries.toDate = toDate.toISOString();
    }
    if (Number.isInteger(Number(limit))) {
      queries.limit = Number(limit);
    }
    if (state && state.toLowerCase() === "closed") {
      queries.status = "COMPLETE,CANCELLED,INVALID,FAILED";
    } else if (state && state.toLowerCase() === "open") {
      queries.status = "ACTIVE,PENDING,CANCEL_PENDING,ACTIVE_PENDING";
    }

    const headers = {};
    headers[API_KEY_HEADER] = this.API_KEY;
    headers[NONCE_HEADER] = utils.generateNonce();
    headers[HOST_HEADER] = utils.getHost(this.BASE_URL);
    headers[SIGNED_HEADER] = utils.constructSignedHeader(headers);

    const url = `${GET_USER_ORDERS(this.USER_ID)}${
      queries.fromDate || queries.toDate || queries.limit
        ? `?${querystring.stringify(queries)}`
        : ""
    }`;

    const signature = utils.calculateSignature("GET", url, this.API_SECRET, {
      headers,
      queries
    });

    headers[SIGNATURE_HEADER] = signature;

    return utils.makeRequest({
      method: "get",
      headers,
      url: `${this.BASE_URL}${url}`
    });
  }

  /**
   *
   * @param {string} orderId
   */
  getOrder(orderId) {
    const headers = {};
    headers[API_KEY_HEADER] = this.API_KEY;
    headers[NONCE_HEADER] = utils.generateNonce();
    headers[HOST_HEADER] = utils.getHost(this.BASE_URL);
    headers[SIGNED_HEADER] = utils.constructSignedHeader(headers);

    const url = GET_USER_ORDER(this.USER_ID, orderId);

    const signature = utils.calculateSignature("GET", url, this.API_SECRET, {
      headers
    });

    headers[SIGNATURE_HEADER] = signature;

    return utils.makeRequest({
      method: "get",
      headers,
      url: `${this.BASE_URL}${url}`
    });
  }

  /**
   *
   * @param {Object.<string, Object>} data
   */
  createOrder(data) {
    if (data && !data.marker_only) {
      data.marker_only = false;
    }
    if (data && !data.order_type) {
      data.order_type = "LIMIT";
    }

    const headers = {};
    headers[API_KEY_HEADER] = this.API_KEY;
    headers[NONCE_HEADER] = utils.generateNonce();
    headers[HOST_HEADER] = utils.getHost(this.BASE_URL);
    headers[CONTENT_TYPE_HEADER] = APPLICATION_JSON;
    headers[SIGNED_HEADER] = utils.constructSignedHeader(headers);

    const url = CREATE_USER_ORDER(this.USER_ID);

    const signature = utils.calculateSignature("POST", url, this.API_SECRET, {
      headers,
      data
    });

    headers[SIGNATURE_HEADER] = signature;

    return utils.makeRequest({
      method: "post",
      headers,
      url: `${this.BASE_URL}${url}`,
      data
    });
  }

  /**
   *
   * @param {string} orderId
   */
  cancelOrder(orderId) {
    const headers = {};
    headers[API_KEY_HEADER] = this.API_KEY;
    headers[NONCE_HEADER] = utils.generateNonce();
    headers[HOST_HEADER] = utils.getHost(this.BASE_URL);
    headers[SIGNED_HEADER] = utils.constructSignedHeader(headers);

    const url = DELETE_USER_ORDER(this.USER_ID, orderId);

    const signature = utils.calculateSignature("DELETE", url, this.API_SECRET, {
      headers
    });

    headers[SIGNATURE_HEADER] = signature;

    return utils.makeRequest({
      method: "delete",
      headers,
      url: `${this.BASE_URL}${url}`
    });
  }
}

module.exports = Incx;
