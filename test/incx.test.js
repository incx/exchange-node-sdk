const assert = require("assert");
const nock = require("nock");
const incx = require("../incx");
const {
  GET_USER_ORDERS,
  GET_USER_ORDER,
  CREATE_USER_ORDER,
  DELETE_USER_ORDER
} = require("../utils/constants");

describe("Incx node sdk test", function() {
  this.timeout(1000);
  const BASE_URL = "http://localhost:3000";
  const USER_ID = "07162b15-0dcd-4e18-ac4c-796d66312b51";
  const ORDER_ID = "16326517-ca88-4d13-a731-eef6e6ab1219";
  const API_KEY = "ad62111c-35de-4e8f-ae34-3300b98ff33f";
  const API_SECRET = "api-secret";
  const REQUIRED_HEADERS = nonce => ({
    "content-type": accept => accept,
    host: host => host,
    "incx-api-key": API_KEY,
    "incx-nonce": nonce,
    "incx-api-signature": signature => signature,
    "incx-signed-headers": SIGNED_HEADERS
  });

  describe("Constructor tests", function() {
    it("create new instance of incx node sdk", done => {
      const config = {
        baseUrl: BASE_URL,
        userId: USER_ID,
        apiKey: API_KEY,
        apiSecret: API_SECRET,
      };
      assert.doesNotThrow(() => {
        new incx(config);
      });
      done();
    });
    it("failed creating new instance of incx node sdk without config", done => {
      assert.throws(() => {
        new incx();
      });
      done();
    });
    it("failed creating new instance of incx node sdk with invalid config", done => {
      const config = {
        userId: USER_ID,
        apiKey: API_KEY,
        apiSecret: API_SECRET,
      };
      assert.throws(() => {
        new incx(config);
      });
      done();
    });
    it("failed creating new instance of incx node sdk with invalid config", done => {
      const config = {
        baseUrl: BASE_URL,
        apiKey: API_KEY,
        apiSecret: API_SECRET,
      };
      assert.throws(() => {
        new incx(config);
      });
      done();
    });
    it("failed creating new instance of incx node sdk with invalid config", done => {
      const config = {
        baseUrl: BASE_URL,
        userId: USER_ID,
        apiSecret: API_SECRET,
      };
      assert.throws(() => {
        new incx(config);
      });
      done();
    });
    it("failed creating new instance of incx node sdk with invalid config", done => {
      const config = {
        baseUrl: BASE_URL,
        userId: USER_ID,
        apiKey: API_KEY,
      };
      assert.throws(() => {
        new incx(config);
      });
      done();
    });
  });

  describe("Get user orders tests", function() {
    afterEach(() => {
      nock.cleanAll();
    });

    it("get user orders", done => {
      const userOrders = [
        {
          user_id: "912ce142-2dc3-4022-9156-eb0982151964",
          created_time: "2018-08-23T19:34:44.589+0000",
          order_id: "ba181ce5-46d5-4f0a-947f-a835c9e1f8de",
          updated_time: "2018-08-23T19:34:44.589+0000",
          base_account_id: null,
          quote_account_id: null,
          fee_credit_account_id: null,
          fee_debit_account_id: null,
          ticker: "TETH/TBTC",
          side: "BUY",
          stp: null,
          order_type: "LIMIT",
          quantity: "1.01",
          quote_coin_amount: null,
          maker_only: false,
          price: "0.005",
          execution_qualifier: null,
          expire_time: "2018-09-23T19:34:44.589+0000",
          status: "PENDING",
          status_invalid_reason: null,
          remaining_quantity: null,
          fees: "3000",
          fee_currency: "TBTC",
          commission: null,
          canary_debug: false
        }
      ];
      nock(BASE_URL, {
        reqheaders: REQUIRED_HEADERS
      })
        .get(GET_USER_ORDERS(USER_ID))
        .reply(200, userOrders);
      const config = {
        baseUrl: BASE_URL,
        userId: USER_ID,
        apiKey: API_KEY,
        apiSecret: API_SECRET,
      };
      const incxSdk = new incx(config);

      incxSdk.getOrders().then(response => {
        assert.deepEqual(response, userOrders);
        done();
      });
    });
    it("get user orders with options", done => {
      const userOrders = [
        {
          base_account_id: "6faf9380-3e24-4ec5-b481-e94bc9843c51",
          canary_debug: false,
          commission: "0.00000000",
          created_time: "2019-02-07T00:30:56.374+0000",
          execution_qualifier: null,
          expire_time: "1970-01-01T00:00:00.000+0000",
          fee_credit_account_id: "ddab916f-12b5-4e28-92f0-f3dd33c55871",
          fee_currency: "TERC",
          fee_debit_account_id: "17a668b9-2363-4f9a-94c3-90daccc706f8",
          fees: "3.68607143",
          maker_only: false,
          order_id: "3c0a718f-5207-4af4-8af8-c8614df58274",
          order_type: "LIMIT",
          partner_fee_credit_account_id: "ae1f096c-0100-4b30-8006-2c2a32998d57",
          partner_fee_currency: "TBTC",
          partner_fees: "0.00082567",
          price: "0.00059133",
          quantity: "349.07253141",
          quote_account_id: "6495bb81-da3f-4dfe-a36c-5d5b99376ae5",
          quote_coin_amount: null,
          remaining_quantity: "349.07253141",
          side: "BUY",
          status: "CANCELLED",
          status_invalid_reason: null,
          stp: null,
          ticker: "TETH/TBTC",
          updated_time: "2019-02-07T00:31:11.685+0000",
          user_id: "65267848-d770-48b8-9fbc-4fb8307ba7dd"
        }
      ];
      nock(BASE_URL, {
        reqheaders: REQUIRED_HEADERS
      })
        .get(GET_USER_ORDERS(USER_ID))
        .query(true)
        .reply(200, userOrders);
      const config = {
        baseUrl: BASE_URL,
        userId: USER_ID,
        apiKey: API_KEY,
        apiSecret: API_SECRET,
        
      };
      const incxSdk = new incx(config);

      incxSdk
        .getOrders({
          fromDate: new Date(),
          toDate: new Date(),
          limit: 25,
          state: 'OPEN'
        }
        )
        .then(response => {
          assert.deepEqual(response, userOrders);
          done();
        });
    });
    it("get user orders failed", done => {
      nock(BASE_URL)
        .get(GET_USER_ORDERS(USER_ID))
        .reply(404, { error: "Not found" });
      const config = {
        baseUrl: BASE_URL,
        userId: USER_ID,
        apiKey: API_KEY,
        apiSecret: API_SECRET,
        
      };
      const incxSdk = new incx(config);

      incxSdk.getOrders().catch(error => {
        assert.equal(error.name, "Error");
        done();
      });
    });
  });

  describe("Get user order tests", function() {
    afterEach(() => {
      nock.cleanAll();
    });

    it("get user order", done => {
      const order = {
        base_account_id: "6faf9380-3e24-4ec5-b481-e94bc9843c51",
        canary_debug: false,
        commission: "0.00000000",
        created_time: "2019-02-07T00:30:56.374+0000",
        execution_qualifier: null,
        expire_time: "1970-01-01T00:00:00.000+0000",
        fee_credit_account_id: "ddab916f-12b5-4e28-92f0-f3dd33c55871",
        fee_currency: "TERC",
        fee_debit_account_id: "17a668b9-2363-4f9a-94c3-90daccc706f8",
        fees: "3.68607143",
        maker_only: false,
        order_id: "3c0a718f-5207-4af4-8af8-c8614df58274",
        order_type: "LIMIT",
        partner_fee_credit_account_id: "ae1f096c-0100-4b30-8006-2c2a32998d57",
        partner_fee_currency: "TBTC",
        partner_fees: "0.00082567",
        price: "0.00059133",
        quantity: "349.07253141",
        quote_account_id: "6495bb81-da3f-4dfe-a36c-5d5b99376ae5",
        quote_coin_amount: null,
        remaining_quantity: "349.07253141",
        side: "BUY",
        status: "CANCELLED",
        status_invalid_reason: null,
        stp: null,
        ticker: "TETH/TBTC",
        updated_time: "2019-02-07T00:31:11.685+0000",
        user_id: "65267848-d770-48b8-9fbc-4fb8307ba7dd"
      };
      nock(BASE_URL, {
        reqheaders: REQUIRED_HEADERS
      })
        .get(GET_USER_ORDER(USER_ID, ORDER_ID))
        .reply(200, order);
      const config = {
        baseUrl: BASE_URL,
        userId: USER_ID,
        apiKey: API_KEY,
        apiSecret: API_SECRET,
        
      };
      const incxSdk = new incx(config);

      incxSdk.getOrder(ORDER_ID).then(response => {
        assert.deepEqual(response, order);
        done();
      });
    });
    it("get user order failed", done => {
      nock(BASE_URL)
        .get(GET_USER_ORDER(USER_ID, ORDER_ID))
        .reply(404, { error: "Not found" });
      const config = {
        baseUrl: BASE_URL,
        userId: USER_ID,
        apiKey: API_KEY,
        apiSecret: API_SECRET,
        
      };
      const incxSdk = new incx(config);

      incxSdk.getOrder(ORDER_ID).catch(error => {
        assert.deepEqual(error.name, "Error");
        done();
      });
    });
    it("get user order failed with no order id", done => {
      nock(BASE_URL)
        .get(GET_USER_ORDER(USER_ID, ORDER_ID))
        .reply(404, { error: "Not found" });
      const config = {
        baseUrl: BASE_URL,
        userId: USER_ID,
        apiKey: API_KEY,
        apiSecret: API_SECRET,
        
      };
      const incxSdk = new incx(config);

      incxSdk.getOrder().catch(error => {
        assert.deepEqual(error.name, "Error");
        done();
      });
    });
  });

  describe("Create user order tests", function() {
    afterEach(() => {
      nock.cleanAll();
    });
    it("create user order", done => {
      const createOrderReqBody = {
        marker_only: false,
        order_type: "LIMIT",
        price: "0.00059133",
        quantity: "349.07253141",
        side: "BUY",
        ticker: "TETH/TBTC"
      };
      nock(BASE_URL, {
        reqheaders: REQUIRED_HEADERS
      })
        .post(CREATE_USER_ORDER(USER_ID))
        .reply(202, { order_id: "826bc58d-87a1-4755-b1f0-135f569e5e03" });
      const config = {
        baseUrl: BASE_URL,
        userId: USER_ID,
        apiKey: API_KEY,
        apiSecret: API_SECRET,
        
      };
      const incxSdk = new incx(config);

      incxSdk.createOrder(createOrderReqBody).then(response => {
        assert.deepEqual(response, {
          order_id: "826bc58d-87a1-4755-b1f0-135f569e5e03"
        });
        done();
      });
    });
    it("create user order not provide optional data fields", done => {
      const createOrderReqBody = {
        price: "0.00059133",
        quantity: "349.07253141",
        side: "BUY",
        ticker: "TETH/TBTC"
      };
      nock(BASE_URL, {
        reqheaders: REQUIRED_HEADERS
      })
        .post(CREATE_USER_ORDER(USER_ID))
        .reply(202, { order_id: "826bc58d-87a1-4755-b1f0-135f569e5e03" });
      const config = {
        baseUrl: BASE_URL,
        userId: USER_ID,
        apiKey: API_KEY,
        apiSecret: API_SECRET,
        
      };
      const incxSdk = new incx(config);

      incxSdk.createOrder(createOrderReqBody).then(response => {
        assert.deepEqual(response, {
          order_id: "826bc58d-87a1-4755-b1f0-135f569e5e03"
        });
        done();
      });
    });
    it("create user order failed", done => {
      nock(BASE_URL)
        .post(CREATE_USER_ORDER(USER_ID))
        .reply(400, { error: "Bad request" });
      const config = {
        baseUrl: BASE_URL,
        userId: USER_ID,
        apiKey: API_KEY,
        apiSecret: API_SECRET,
        
      };
      const incxSdk = new incx(config);

      incxSdk.createOrder({}).catch(error => {
        assert.deepEqual(error.name, "Error");
        done();
      });
    });
  });

  describe("Cancle user order tests", function() {
    afterEach(() => {
      nock.cleanAll();
    });
    it("Cancle user order", done => {
      nock(BASE_URL, {
        reqheaders: REQUIRED_HEADERS
      })
        .delete(DELETE_USER_ORDER(USER_ID, ORDER_ID))
        .reply(202, { message: "Request to cancel order is accepted" });
      const config = {
        baseUrl: BASE_URL,
        userId: USER_ID,
        apiKey: API_KEY,
        apiSecret: API_SECRET,
        
      };
      const incxSdk = new incx(config);

      incxSdk.cancelOrder(ORDER_ID).then(response => {
        assert.deepEqual(response, {
          message: "Request to cancel order is accepted"
        });
        done();
      });
    });

    it("Cancle user order failed", done => {
      nock(BASE_URL)
        .delete(DELETE_USER_ORDER(USER_ID, ORDER_ID))
        .reply(400, { error: "Bad request" });
      const config = {
        baseUrl: BASE_URL,
        userId: USER_ID,
        apiKey: API_KEY,
        apiSecret: API_SECRET,
        
      };
      const incxSdk = new incx(config);

      incxSdk.cancelOrder().catch(error => {
        assert.deepEqual(error.name, "Error");
        done();
      });
    });
  });
});
