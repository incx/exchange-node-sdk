const assert = require("assert");
const querystring = require("querystring");
const utils = require("../../utils/utils");
const {
  GET_USER_ORDERS,
  GET_USER_ORDER,
  CREATE_USER_ORDER,
  DELETE_USER_ORDER
} = require("../../utils/constants");

describe("Incx node sdk utils tests", function() {
  this.timeout(1000);
  const BASE_URL = "http://localhost:3000";
  const USER_ID = "07162b15-0dcd-4e18-ac4c-796d66312b51";
  const ORDER_ID = "16326517-ca88-4d13-a731-eef6e6ab1219";
  const API_KEY = "ad62111c-35de-4e8f-ae34-3300b98ff33f";
  const API_SECRET = "secret";
  const REQUIRED_HEADERS = nonce => ({
    "content-type": accept => accept,
    host: host => host,
    "incx-api-key": API_KEY,
    "incx-nonce": nonce,
    "incx-api-signature": signature => signature,
    "incx-signed-headers": SIGNED_HEADERS
  });
  
  it("calculate signature - get orders test", done => {
    const headers = {
      host: "localhost",
      "incx-api-key": "ad62111c-35de-4e8f-ae34-3300b98ff33f",
      "incx-nonce": "1549928240315",
      "incx-signed-headers": "host;incx-api-key;incx-nonce;incx-signed-headers"
    };
    const queries = {
      fromDate: "2019-02-11T23:48:35.708Z",
      limit: 25,
      state: "ACTIVE,PENDING,CANCEL_PENDING,ACTIVE_PENDING",
      toDate: "2019-02-11T23:48:35.708Z"
    };
    const options = {
        headers,
        queries,
    }
    const url = `${GET_USER_ORDERS(USER_ID)}?${querystring.stringify(queries)}`;
    const expectedSignature = "bcef7ac3a601d8f6952b82a42be715bc9db73b9beaaed020576015ef763e7ed2";
    const signature = utils.calculateSignature(
      "GET",
      url,
      API_SECRET,
      options
    );
    assert.equal(signature, expectedSignature);
    done();
  });
  it("calculate signature - get orders failed test", done => {
    const headers = {
      host: "differenthost",
      "incx-api-key": "ad62111c-35de-4e8f-ae34-3300b98ff33f",
      "incx-nonce": "1549928240315",
      "incx-signed-headers": "host;incx-api-key;incx-nonce;incx-signed-headers"
    };
    const queries = {
      fromDate: "2019-02-11T23:48:35.708Z",
      limit: 25,
      state: "ACTIVE,PENDING,CANCEL_PENDING,ACTIVE_PENDING",
      toDate: "2019-02-11T23:48:35.708Z"
    };
    const options = {
        headers,
        queries,
    }
    const url = `${GET_USER_ORDERS(USER_ID)}?${querystring.stringify(queries)}`;
    const expectedSignature = "bcef7ac3a601d8f6952b82a42be715bc9db73b9beaaed020576015ef763e7ed2";
    const signature = utils.calculateSignature(
      "GET",
      url,
      API_SECRET,
      options
    );
    assert.notEqual(signature, expectedSignature);
    done();
  });

  it("calculate signature test - get order test", done => {
    const headers = {
      host: "localhost",
      "incx-api-key": "ad62111c-35de-4e8f-ae34-3300b98ff33f",
      "incx-nonce": "1549928240315",
      "incx-signed-headers": "host;incx-api-key;incx-nonce;incx-signed-headers"
    };
    const options = {
        headers,
    }
    const url = `${GET_USER_ORDER(USER_ID, ORDER_ID)}`;
    const expectedSignature = "08c5efae94fb3941e5d802b3f46d6b0ceafc7f2edf2de5d912d02711431ed88e";
    const signature = utils.calculateSignature(
      "GET",
      url,
      API_SECRET,
      options
    );
    assert.equal(signature, expectedSignature);
    done();
  });
  it("calculate signature test - get order failed test", done => {
    const headers = {
      host: "differentlocalhost",
      "incx-api-key": "ad62111c-35de-4e8f-ae34-3300b98ff33f",
      "incx-nonce": "1549928240315",
      "incx-signed-headers": "host;incx-api-key;incx-nonce;incx-signed-headers"
    };
    const options = {
        headers,
    }
    const url = `${GET_USER_ORDER(USER_ID, ORDER_ID)}`;
    const expectedSignature = "08c5efae94fb3941e5d802b3f46d6b0ceafc7f2edf2de5d912d02711431ed88e";
    const signature = utils.calculateSignature(
      "GET",
      url,
      API_SECRET,
      options
    );
    assert.notEqual(signature, expectedSignature);
    done();
  });

  it("calculate signature test - create order test", done => {
    const headers = {
      "content-type": "application/json",
      host: "localhost",
      "incx-api-key": "ad62111c-35de-4e8f-ae34-3300b98ff33f",
      "incx-nonce": "1549928240315",
      "incx-signed-headers": "host;incx-api-key;incx-nonce;incx-signed-headers"
    };
    const data = {
        marker_only: false,
        order_type: "LIMIT",
        price: "0.00059133",
        quantity: "349.07253141",
        side: "BUY",
        ticker: "TETH/TBTC"
    }
    const options = {
        headers,
        data
    }
    const url = `${CREATE_USER_ORDER(USER_ID)}`;
    const expectedSignature = "ff529cd31d8ca4cc533cf2ea5e817255a145949538f10add681703a04c343b0a";
    const signature = utils.calculateSignature(
      "POST",
      url,
      API_SECRET,
      options
    );
    assert.equal(signature, expectedSignature);
    done();
  });
  it("calculate signature test - create order failed test", done => {
    const headers = {
      "content-type": "application/json",
      host: "differentlocalhost",
      "incx-api-key": "ad62111c-35de-4e8f-ae34-3300b98ff33f",
      "incx-nonce": "1549928240315",
      "incx-signed-headers": "host;incx-api-key;incx-nonce;incx-signed-headers"
    };
    const data = {
        marker_only: false,
        order_type: "LIMIT",
        price: "0.00059133",
        quantity: "349.07253141",
        side: "BUY",
        ticker: "TETH/TBTC"
    }
    const options = {
        headers,
        data
    }
    const url = `${CREATE_USER_ORDER(USER_ID)}`;
    const expectedSignature = "ff529cd31d8ca4cc533cf2ea5e817255a145949538f10add681703a04c343b0a";
    const signature = utils.calculateSignature(
      "POST",
      url,
      API_SECRET,
      options
    );
    assert.notEqual(signature, expectedSignature);
    done();
  });

  it("calculate signature test - delete order test", done => {
    const headers = {
      "content-type": "application/json",
      host: "localhost",
      "incx-api-key": "ad62111c-35de-4e8f-ae34-3300b98ff33f",
      "incx-nonce": "1549928240315",
      "incx-signed-headers": "host;incx-api-key;incx-nonce;incx-signed-headers"
    };
    const options = {
        headers,
    }
    const url = `${DELETE_USER_ORDER(USER_ID, ORDER_ID)}`;
    const expectedSignature = "baf1371dec0711d246f8a7bb3c9d0b462455082f03e0dc519a5e3bc43ea45c2b";
    const signature = utils.calculateSignature(
      "DELETE",
      url,
      API_SECRET,
      options
    );
    assert.equal(signature, expectedSignature);
    done();
  });
  it("calculate signature test - delete order failed test", done => {
    const headers = {
      "content-type": "application/json",
      host: "differentlocalhost",
      "incx-api-key": "ad62111c-35de-4e8f-ae34-3300b98ff33f",
      "incx-nonce": "1549928240315",
      "incx-signed-headers": "host;incx-api-key;incx-nonce;incx-signed-headers"
    };
    const options = {
        headers,
    }
    const url = `${DELETE_USER_ORDER(USER_ID, ORDER_ID)}`;
    const expectedSignature = "baf1371dec0711d246f8a7bb3c9d0b462455082f03e0dc519a5e3bc43ea45c2b";
    const signature = utils.calculateSignature(
      "DELETE",
      url,
      API_SECRET,
      options
    );
    assert.notEqual(signature, expectedSignature);
    done();
  });
});
