const API_KEY_HEADER = "incx-api-key";
const NONCE_HEADER = "incx-nonce";
const SIGNATURE_HEADER = "incx-api-signature";
const SIGNED_HEADER = "incx-signed-headers";
const HOST_HEADER = "host";
const CONTENT_TYPE_HEADER = "content-type";
const APPLICATION_JSON = "application/json";

const GET_USER_ORDERS = userId => `/v1/users/${userId}/orders`;
const GET_USER_ORDER = (userId, orderId) =>
  `/v1/users/${userId}/orders/${orderId}`;
const CREATE_USER_ORDER = userId => `/v1/users/${userId}/orders`;
const DELETE_USER_ORDER = (userId, orderId) =>
  `/v1/users/${userId}/orders/${orderId}`;
  
module.exports = {
    API_KEY_HEADER,
    NONCE_HEADER,
    SIGNATURE_HEADER,
    SIGNED_HEADER,
    HOST_HEADER,
    GET_USER_ORDERS,
    GET_USER_ORDER,
    CREATE_USER_ORDER,
    DELETE_USER_ORDER,
    CONTENT_TYPE_HEADER,
    APPLICATION_JSON,
}