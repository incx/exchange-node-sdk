var url = require("url");
const axios = require("axios");
const crypto = require("crypto");
const { SIGNED_HEADER, NONCE_HEADER } = require("./constants");

const SUPPORTED_ALGORITHM = {
  "HMAC-SHA256": secret => {
    return crypto.createHmac("sha256", secret);
  }
};
const DEFAULT_ALGORITHM = "HMAC-SHA256";
const DEFAULT_KEY_OFFSET = "INCX";

function validateConfig(config) {
  if (!config) {
    throw new Error("Config is required");
  } else if (!config.baseUrl) {
    throw new Error("Required config: 'baseUrl'");
  } else if (!config.userId) {
    throw new Error("Required config: 'userId'");
  } else if (!config.apiKey) {
    throw new Error("Required config: 'apiKey'");
  } else if (!config.apiSecret) {
    throw new Error("Required config: 'apiSecret'");
  }
}

function getHost(urlStr) {
  const l = url.parse(urlStr);
  return l.hostname;
}

function generateNonce() {
  return new Date().valueOf().toString();
}

function constructSignedHeader(headers) {
  let signedHeaders = "";
  headers[SIGNED_HEADER] = "";
  Object.keys(headers)
    .sort()
    .forEach((header, index, array) => {
      if (index < array.length - 1) {
        signedHeaders += `${header};`;
      } else {
        signedHeaders += header;
      }
    });
  return signedHeaders;
}

function makeRequest(request) {
  return axios(request)
    .then(res => {
      return res.data || {};
    })
    .catch(error => {
      throw error;
    });
}

/**
 *
 * @param {string} method
 * @param {string} url
 * @param {string} apiSecret
 * @param {*=} options
 */
function calculateSignature(method, url, apiSecret, options) {
  const { headers, queries, data } = options;

  // Create canonical req url
  const urlEncoded = `${encodeURI(encodeURI(url))}`;

  // Create canonical req queries
  let canonicalQueries = "";
  Object.keys(queries || {})
    .sort()
    .forEach((key, i, array) => {
      if (i < array.length - 1) {
        canonicalQueries += `${encodeURIComponent(key)}=${encodeURIComponent(
          queries[key] || ""
        )}&`;
      } else {
        canonicalQueries += `${encodeURIComponent(key)}=${encodeURIComponent(
          queries[key] || ""
        )}`;
      }
    });

  let canonicalHeaders = "";
  Object.keys(headers)
    .sort()
    .forEach((header, index, array) => {
      if (index < array.length - 1) {
        canonicalHeaders += `${header.toLowerCase()}:${(
          headers[header] || ""
        ).trim()}\n`;
      } else {
        canonicalHeaders += `${header.toLowerCase()}:${(
          headers[header] || ""
        ).trim()}`;
      }
    });

  // Create canonical req payload, HexEncode(SHA256_Hash(RequestPayloadAsString))
  let sha256 = crypto.createHash("sha256");
  const hexHashedPayload = `${sha256
    .update(JSON.stringify(data||''))
    .digest("hex").toLowerCase()}`;

  // Construct canonical req from components
  const canonicalReq =
    `${method.toLowerCase()}\n${urlEncoded}\n${canonicalQueries && `${canonicalQueries}\n`}` +
    `${canonicalHeaders}\n${hexHashedPayload}`;

  // Create string to sign
  sha256 = crypto.createHash("sha256");
  const stringToSign = `${DEFAULT_ALGORITHM}\n${sha256
    .update(canonicalReq)
    .digest("hex")}`;

  // Derive signing key
  let hmac = SUPPORTED_ALGORITHM[DEFAULT_ALGORITHM](
    `${DEFAULT_KEY_OFFSET}${apiSecret}`
  );
  const kSigning = hmac.update(headers[NONCE_HEADER]).digest("base64");

  // Derive signature
  hmac = SUPPORTED_ALGORITHM[DEFAULT_ALGORITHM](kSigning);
  return hmac.update(stringToSign).digest("hex");
}

module.exports = {
  validateConfig,
  getHost,
  generateNonce,
  constructSignedHeader,
  makeRequest,
  calculateSignature
};
